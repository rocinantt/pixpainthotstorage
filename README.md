## SETUP

### python deps
> cd backend/src
> 
> poetry install

### tarantool deps

> cd backend/src
> 
> tarantoolctl rocks install ddl

## RUN

### python
> cd backend/src
> 
> python3 main.py


### tarantool
> cd backend/src
> 
> tarantool init.lua