local yaml = require('yaml')
local ddl = require('ddl')
local fiber = require('fiber')

box.cfg {
    memtx_memory = 4096 * 1024 * 1024,
    memtx_max_tuple_size = 2048 * 1024,
    listen = 3301
}



function batch_insert(space_name, list)
    local space = box.space[space_name]
    box.begin()
    for _, record in ipairs(list) do
        space:insert(record)
    end
    box.commit()
end

function batch_update(space_name, list)
    local space = box.space[space_name]
    box.begin()
    for _, record in ipairs(list) do
        space:update(record[1], record[2])
    end
    box.commit()

end

function batch_upsert(space_name, list)
    local space = box.space[space_name]
    box.begin()
    for _, record in ipairs(list) do
        space:upsert(record[1], record[2])
    end
    box.commit()
    fiber.yield()
end

function batch_delete(space_name, keys)
    local space = box.space[space_name]
    box.begin()
    for _, k in ipairs(keys) do
        space:delete(k)
    end
    box.commit()
end

function delete_all(space_name)
    local space = box.space[space_name]
    space:truncate()
end

function create_user()
    box.schema.user.grant('guest', 'read,write,execute', 'universe')
end



function setup()

    pcall(create_user)

    local fh = io.open('schema.yml', 'r')
    local schema = yaml.decode(fh:read('*all'))
    fh:close()
    local ok, err = ddl.check_schema(schema)
    if not ok then
        print(err)
    end
    local ok, err = ddl.set_schema(schema)
    if not ok then
        print(err)
    end
end


if box.space.mosaic == nil then
    setup()
end
