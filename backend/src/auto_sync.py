from services.replication import ListenTransactionService
from services.db_sync import DBSyncService
from config.db import db_settings

if __name__ == '__main__':
    sync_service = DBSyncService()
    ListenTransactionService(
        **db_settings
    ).execute(sync_service.sync)
