from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from routers.museums import router as museums_router
from routers.webhooks import router as webhooks_router

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=['*'],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"]
)

app.include_router(museums_router)
app.include_router(webhooks_router)
