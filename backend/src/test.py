import asyncio as aio

from services.testing import CreateMosaicService, CreateMosaicPixelsService
from services.museums import MosaicPixelCRUDService
from schemas.museums import MosaicPixel


async def test():
    mosaics = await CreateMosaicService().execute(1, 1000, 1000)
    mosaic = mosaics[0]
    await CreateMosaicPixelsService().execute(mosaic, 0.8)




if __name__ == "__main__":
    loop = aio.get_event_loop()
    loop.run_until_complete(test())
