import asyncio as aio

from services.db_sync import ForceDBSyncService
from config.db import db_settings

if __name__ == '__main__':
    service = ForceDBSyncService(**db_settings)
    future = service.sync()
    loop = aio.get_event_loop()
    loop.run_until_complete(future)
