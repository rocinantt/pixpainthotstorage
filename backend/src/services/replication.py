import json
import typing as t

import psycopg2
from psycopg2.extras import (
    LogicalReplicationConnection,
    StopReplication,
    ReplicationMessage
)

from services.base import AbstractService
from schemas.replication import DBTransaction

Callback = t.NewType("Callback", t.Callable[[t.List[DBTransaction]], None])


class TransactionConsumer:

    def __init__(self, cb: Callback):
        self.cb = cb

    @staticmethod
    def parse_message(msg: str) -> t.List[DBTransaction]:
        result = []
        message = json.loads(msg)
        changes = message.get("change", [])
        for c in changes:
            data = dict()
            if c.get("columnnames", None):
                for (k, v) in zip(c["columnnames"], c["columnvalues"]):
                    data[k] = v

            elif c.get("oldkeys", None):
                for (k, v) in zip(c["oldkeys"]["keynames"], c["oldkeys"]["keyvalues"]):
                    data[k] = v

            transaction = DBTransaction(
                table=c['table'],
                operation=c["kind"],
                data=data
            )
            result.append(transaction)
        return result

    def __call__(self, msg: ReplicationMessage):
        transactions = self.parse_message(msg.payload)
        msg.cursor.send_feedback(flush_lsn=msg.data_start)
        self.cb(transactions)


class ListenTransactionService(AbstractService):

    def __init__(self, dbname: str, user: str, password: str, host: str, port: int):
        self.conn = psycopg2.connect(
            dbname=dbname,
            user=user,
            password=password,
            host=host,
            port=port,
            connection_factory=LogicalReplicationConnection
        )
        self.slot_name = "replication_slot"

    def execute(self, callback: Callback):
        consumer = TransactionConsumer(callback)
        cur = self.conn.cursor()

        try:
            cur.start_replication(slot_name=self.slot_name, decode=True)
        except psycopg2.ProgrammingError:
            cur.create_replication_slot(self.slot_name, output_plugin='wal2json')
            cur.start_replication(slot_name=self.slot_name, decode=True)

        try:
            cur.consume_stream(consumer)
        except (KeyboardInterrupt, StopReplication):
            pass
        finally:
            cur.drop_replication_slot(self.slot_name)
