from itertools import product

from services.museums import MosaicCRUDService, MosaicPixelCRUDService
from services.base import AbstractService
from schemas.museums import Mosaic, MosaicPixel


class CreateMosaicService(AbstractService):

    async def execute(self, count: int, max_x: int, max_y: int):
        mosaics = [Mosaic(i, max_x, max_y) for i in range(count)]
        await MosaicCRUDService().bulk_create(mosaics)
        return mosaics


class CreateMosaicPixelsService(AbstractService):

    async def execute(self, mosaic: Mosaic, percentage: float):
        assert 0 < percentage <= 1.0
        mosaic_size = mosaic.max_x * mosaic.max_y
        to_create_count = int(mosaic_size * percentage)
        to_create = []
        for i, (x, y) in enumerate(product(range(mosaic.max_x), range(mosaic.max_y))):
            if len(to_create) >= to_create_count:
                break
            to_create.append(MosaicPixel(
                id=i, x=x, y=y,
                mosaic_id=mosaic.id,
                user_id=1,
                created_at=1234,
                group_id="asda",
                status="purchased"
            ))
        await MosaicPixelCRUDService().bulk_create(to_create)


