import abc
import typing as t

from config.db import session


class AbstractService(abc.ABC):

    @abc.abstractmethod
    def execute(self, *args):
        ...


class AbstractCRUDService(abc.ABC):

    @abc.abstractmethod
    def get(self, *args):
        ...

    @abc.abstractmethod
    def create(self, *args):
        ...


class TarantoolCRUDService(AbstractCRUDService):
    primary_key = "id"

    @property
    @abc.abstractmethod
    def space(self) -> str:
        ...

    @property
    @abc.abstractmethod
    def schema(self) -> t.Type[t.NamedTuple]:
        ...

    def _build_update_request(self, obj: schema, lua_idx: bool = False) -> t.List[list]:
        request = list()
        for idx, (k, v) in enumerate(obj._asdict().items()):
            if k == self.primary_key:
                continue
            request.append(["=", idx+1 if lua_idx else idx, v])
        return request

    async def delete(self, pk: int) -> None:
        async with session() as s:
            await s.delete(self.space, [pk])

    async def bulk_delete(self, ids: t.List[int]) -> None:
        async with session() as s:
            await s.call("batch_delete", (self.space, ids))

    async def update(self, obj: schema):
        existing_ids = await self.get_existing_ids((getattr(obj, self.primary_key),))
        if len(existing_ids) == 0:
            await self.create(obj)
        else:
            update_request = self._build_update_request(obj)
            async with session() as s:
                await s.update(self.space, [getattr(obj, self.primary_key)], update_request)

    async def bulk_update(self, objects: list):
        update_request = [
            (getattr(obj, self.primary_key),
             self._build_update_request(obj, True))
            for obj in objects
        ]
        async with session() as s:
            await s.call("batch_update", (self.space, update_request))

    async def bulk_upsert(self, objects: list):
        update_request = [
            (tuple(obj),
             self._build_update_request(obj, True))
            for obj in objects
        ]

        async with session() as s:
            await s.call("batch_upsert", (self.space, update_request))

    async def create(self, obj: schema) -> t.Optional[tuple]:
        existing_ids = await self.get_existing_ids((getattr(obj, self.primary_key),))
        if len(existing_ids) != 0:
            return None
        async with session() as s:
            response = await s.insert(self.space, tuple(obj))
        return tuple(response.body[0])

    async def bulk_create(self, objects: list):
        # existing_ids = await self.get_existing_ids(tuple(o[0] for o in objects))
        # not_existing_objects = filter(lambda o: o[0] not in existing_ids, objects)
        async with session() as s:
            await s.call("batch_insert", (self.space, objects))

    async def get_existing_ids(self, ids: t.Tuple[int]) -> t.Tuple[int]:

        str_ids = str(ids) if len(ids) > 1 else f"({ids[0]})"
        sql_expr = """
        SELECT "%s" FROM "%s" WHERE "%s" IN %s
        """ % (self.primary_key, self.space, self.primary_key, str_ids)

        async with session() as s:
            response = await s.sql(sql_expr)
        return tuple(i[0] for i in response.body)

    async def get(self, pk: int) -> t.Optional[tuple]:
        async with session() as s:
            response = await s.select(
                self.space,
                index=self.primary_key,
                key=[pk]
            )
        if len(response.body) == 0:
            return None
        return tuple(response.body[0])

    async def all(self) -> t.List[tuple]:
        async with session() as s:
            response = await s.select(self.space)
        return [tuple(i) for i in response.body]
