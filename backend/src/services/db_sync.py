import sys
import asyncio as aio
from itertools import takewhile
from typing import List, Type, NamedTuple, Generator

import psycopg2

from services.museums import MosaicPixelCRUDService, MosaicCRUDService
from schemas.museums import MosaicPixel, Mosaic
from schemas.replication import DBTransaction

mappings = {
    "museums_mosaic": {"schema": Mosaic, "crud": MosaicCRUDService()},
    "museums_mosaicpixel": {"schema": MosaicPixel, "crud": MosaicPixelCRUDService()},
}

sys.setrecursionlimit(5000)


class DBSyncService:
    def sync(self, transactions: List[DBTransaction]):
        for txn in transactions:
            mapping = mappings.get(txn.table, None)
            if not mapping:
                continue

            future = None
            if txn.operation == "delete":
                future = mapping["crud"].delete(txn.data.get("id"))
            else:
                as_schema = self._data_as_schema(txn.data, mapping["schema"])
                if txn.operation == "insert":
                    future = mapping["crud"].create(as_schema)
                else:
                    future = mapping["crud"].update(as_schema)

            if future:
                loop = aio.get_event_loop()
                loop.run_until_complete(future)

    @staticmethod
    def _data_as_schema(data: dict, schema: Type[NamedTuple]):
        schema_fields = dict()
        for field in schema._fields:
            schema_fields[field] = data[field]
        return schema(**schema_fields)


class ForceDBSyncService:
    def __init__(self, dbname: str, user: str, password: str, host: str, port: int):
        self.conn = psycopg2.connect(
            dbname=dbname,
            user=user,
            password=password,
            host=host,
            port=port,
        )

    def _fetch_rows(self, table: str, limit: int, offset: int) -> List[NamedTuple]:
        with self.conn.cursor() as curs:
            fields = mappings[table]['schema']._fields
            curs.execute(
                "SELECT %s FROM %s LIMIT %i OFFSET %i"
                % (",".join(fields), table, limit, offset)
            )
            rows = curs.fetchall()
        schema = mappings[table]["schema"]
        return [schema(*row) for row in rows]

    def rows_generator(
            self,
            table: str,
            step: int = 100_000
    ) -> Generator[List[tuple], None, None]:
        offset = 0
        limit = step
        while True:
            yield self._fetch_rows(table, offset=offset, limit=limit)
            offset = limit
            limit += step

    async def sync(self):
        for table in mappings.keys():
            generator = self.rows_generator(table, step=1_000)
            for rows in takewhile(lambda result: len(result) != 0, generator):
                print('fetched')
                await mappings[table]["crud"].bulk_upsert(rows)
                print('inserted')
