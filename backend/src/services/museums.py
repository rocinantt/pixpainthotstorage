import random
from typing import List, Optional, Tuple
from utils import timeit, timing

import numpy as np
from asynctnt import TarantoolTuple

from services.base import TarantoolCRUDService, AbstractService
from schemas.museums import MosaicPixel, Mosaic, PixelAreaRequest, Point
from config.db import session


class MosaicCRUDService(TarantoolCRUDService):
    space = "mosaic"
    schema = Mosaic


class MosaicPixelCRUDService(TarantoolCRUDService):
    space = "mosaic_pixel"
    schema = MosaicPixel


class GetMosaicAreaPixelsService(AbstractService):
    space = "mosaic_pixel"
    index_key = "x_y"

    @timeit
    async def execute(self, data: PixelAreaRequest) -> List[TarantoolTuple]:
        sql_expr = self._build_sql(data)
        async with session() as s:
            response = await s.eval(f"""return box.execute([[{sql_expr}]])""")
        return response.body[0]['rows']

    def _build_sql(self, d: PixelAreaRequest):
        return """ 
            SELECT "id",
                    "x",
                    "y",
                    "group_id",
                    "user_id",
                    "mosaic_id",
                    "status"
            FROM "%s"
            INDEXED BY "%s"
            WHERE ("mosaic_id" = %i
                    AND "x" >= %i
                    AND "x" <= %i
                    AND "y" >= %i
                    AND "y" <= %i)
        """ % (self.space, self.index_key, d.mosaic_pk, d.x1, d.x2, d.y1, d.y2)


class GetMosaicFreePixelService(AbstractService):
    @timeit
    async def execute(self, mosaic_pk: int) -> Optional[Point]:
        unfree_pixels = await self._get_unfree_pixels(mosaic_pk)
        mosaic_size = await self._get_mosaic_size(mosaic_pk)
        if mosaic_size is None:
            return None
        max_x, max_y = mosaic_size
        if len(unfree_pixels) == 0:
            return Point(random.randint(0, max_x), random.randint(0, max_y))
        return self._get_free_pixel(max_x, max_y, unfree_pixels)

    @timing
    def _get_free_pixel(self, max_x: int, max_y: int, unfree_pixels: List[tuple]) -> Optional[Point]:
        total_mosaic_area = np.zeros((max_x, max_y))
        unfree_points = tuple(zip(*unfree_pixels))
        total_mosaic_area[unfree_points[0], unfree_points[1]] = 1
        free_pixel = np.where(total_mosaic_area == 0)
        free_pixel = tuple(zip(*free_pixel))
        if len(free_pixel) != 0:
            return Point(free_pixel[0][0], free_pixel[0][1])
        return None

    @timeit
    async def _get_unfree_pixels(self, mosaic_pk: int) -> List[tuple]:
        sql_expr = self._build_sql(mosaic_pk)
        async with session() as s:
            response = await s.sql(sql_expr, parse_metadata=False)
        return response.body

    @staticmethod
    async def _get_mosaic_size(mosaic_pk: int) -> Optional[Tuple[int, int]]:
        async with session() as s:
            response = await s.select("mosaic", [mosaic_pk])
        data = response.body
        if len(data) == 0:
            return None
        return data[0]["max_x"], data[0]["max_y"]

    @staticmethod
    def _build_sql(mosaic_pk: int):
        return """
            SELECT "x","y"
            FROM "%s" 
            INDEXED BY "%s"
            WHERE ("mosaic_id" = %i) 
            AND ("status" != 'free')
        """ % ("mosaic_pixel", "pixel_status", mosaic_pk)
