import typing as t
from dataclasses import dataclass


@dataclass
class DBTransaction:
    table: str
    operation: t.Literal["insert", "delete", "update"]
    data: dict
