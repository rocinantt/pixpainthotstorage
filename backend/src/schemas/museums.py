from typing import NamedTuple

from pydantic import BaseModel

from schemas.base import Status


class Point(NamedTuple):
    x: int
    y: int


class Mosaic(NamedTuple):
    id: int
    max_x: int
    max_y: int


class MosaicPixel(NamedTuple):
    id: int
    x: int
    y: int
    group_id: str
    user_id: int
    mosaic_id: int
    status: Status


class PixelAreaRequest(BaseModel):
    mosaic_pk: int
    x1: int
    y1: int
    x2: int
    y2: int


class PixelAreaResponse(BaseModel):
    id: int
    user_id: int
    x: int
    y: int
    status: Status
