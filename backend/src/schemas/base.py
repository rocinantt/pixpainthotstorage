from typing import Literal, NewType

Status = NewType('Status', Literal["free", "hold", "purchased"])
