from typing import List, Optional

from fastapi import APIRouter
from asynctnt import TarantoolTuple

from schemas.museums import PixelAreaRequest, MosaicPixel, Point, Mosaic
from services.museums import MosaicPixelCRUDService, GetMosaicAreaPixelsService
from services.museums import GetMosaicFreePixelService, MosaicCRUDService

router = APIRouter(prefix='/api/museums')


@router.get('/pixels/area/', response_model=list)
async def get_pixels_area(mosaic_pk: int, x1: int, y1: int, x2: int, y2: int):
    data = PixelAreaRequest(mosaic_pk=mosaic_pk, x1=x1, x2=x2, y1=y1, y2=y2)
    return await GetMosaicAreaPixelsService().execute(data)


@router.get('/pixels/free/', response_model=Optional[Point])
async def get_mosaic_free_pixel(mosaic_pk: int):
    return await GetMosaicFreePixelService().execute(mosaic_pk)



