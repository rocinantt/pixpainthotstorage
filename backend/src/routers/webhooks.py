from typing import List

from fastapi import APIRouter

from services.museums import MosaicPixelCRUDService, MosaicCRUDService
from schemas.museums import Mosaic, MosaicPixel

router = APIRouter(prefix='/api/webhooks')


@router.post('/pixels/create/')
async def create_pixels(data: List[MosaicPixel]):
    await MosaicPixelCRUDService().bulk_create(data)
    return {"ok": True}


@router.post('/pixels/update/')
async def update_pixels(data: List[MosaicPixel]):
    await MosaicPixelCRUDService().bulk_update(data)
    return {"ok": True}


@router.post('/pixels/delete/')
async def delete_pixels(data: List[int]):
    await MosaicPixelCRUDService().bulk_delete(data)
    return {"ok": True}


@router.post('/mosaics/create/')
async def create_mosaics(data: List[Mosaic]):
    await MosaicCRUDService().bulk_create(data)
    return {"ok": True}


@router.post('/mosaics/update/')
async def update_mosaics(data: List[Mosaic]):
    await MosaicCRUDService().bulk_update(data)
    return {"ok": True}


@router.post('/mosaics/delete/')
async def delete_mosaics(data: List[int]):
    await MosaicCRUDService().bulk_delete(data)
    return {"ok": True}
