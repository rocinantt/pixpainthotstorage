import os
from typing import TypedDict

import asynctnt


def session():
    return asynctnt.Connection(host='tarantool', port=3301)


class DBSettings(TypedDict):
    dbname: str
    user: str
    password: str
    host: str
    port: str


db_settings = DBSettings(
    dbname=os.getenv("DB_NAME", 'postgres'),
    user=os.getenv("DB_USER", 'postgres'),
    password=os.getenv("DB_PASSWORD", 'nopassword'),
    host=os.getenv("DB_HOST", 'localhost'),
    port=os.getenv("DB_PORT", "5433"),
)
